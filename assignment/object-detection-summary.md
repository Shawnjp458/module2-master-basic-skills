**Object Detection**

---------------------------------------------------------------------------------------------------------------------------

Object detection is a computer vision technique that uses the special features of an object in classifying the class. It can be employed to identify and locate objects in a particular scene and accurately label them.

Object detection has applications in many areas of computer vision, including image retrieval and video surveillance.

**RCNN**

Object Detection nowadays solely relies on the use of RCNN(Region-Based Convolutional Network). Instead of using a basic CNN (which would result in a large number of regions), the RCNN method places a few boxes in the images and then checks if any of these have a object in them.

![Object Detection](objdetect.PNG)


**How it works**

1. First we take an image as imput.

2. Next the RCNN generates sub-segmentations so that the image is divided into multiple regions.

3. Then it looks for similar regions and combines then to form a larger region.
This is done by taking multiple factors into consideration such as color similarity,size similarity and so on.

4. Once the regions cannot be further enlarged, we obtain the final region of interest(ROI)

5. After getting the regions we use SVM to classify the objects and the background.

6. For more accurate classification of the ROI, we can use a linear regression model.

-----------------------------------------------------------------------------------------------------------------------------------------------------

**Shelf Watch**

ShelfWatch is an automated retail shelf monitoring tool that helps retailers achieve the desired in-store execution and compliance.

![ShelfWatch](shelfwatch.PNG)

**How it Works**


- Retail Employee capture the image of the shelf and upload it on the app.


- The images are sent for analysis on ShelfWatch Cloud.


- Within a few minutes the retail employees get insights regarding the shelfspace and the management team receives a detailed analysis.

![Shelf Object](shelfwatch2.PNG)

**Advantages**


- Instant response to the situation inside the retail outlet.


- Low cost and research turnaround time allows ShelfWatch to be scalable.

- Blurred images can be avoided as ShelfWatch provides an alert if the image is not distinguishable.

