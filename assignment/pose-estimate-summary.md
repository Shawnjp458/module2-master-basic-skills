**Pose Estimation**

---------------------------------------------------------------------------------------------------------------------------------------------------

*Pose Estimation* can be defined as the computer vision techniques used to predict the location of various keypoints(joints). This is done by looking at a combination of the pose and the orientation of a given person/object.

Pose Estimation can be used in full-body gesture control and even detecting poses during physical exercises. There are a number of challenging problems to be overcome due to the wide variety of possible poses, numerous degrees of freedom and a variety of appearances or outfits. 

**How it works**

There are 2 main approaches in pose estimation:

- Top down approach
- Bottom Up approach

The top down approach starts by identifying the pose using a bounding box object detctor.Here the processing is done from low to high resolutions

Whereas in bottom up approach, the identity-free entities are localized and then they are grouped according to poses.



-------------------------------------------------------------------------------------------------------------------------------------------------------

At the CVPR(Conference on Computer Vision and Pattern Recognition) 2020, Google researchers unveiled a new approach to body pose estimation, *Blazepose*.

**BlazePose**

BlazePose is a lightweight convolutional neural network architecture for human pose estimation that is tailored for real-time inference on mobile devices.

![Blazepose](blazepose.PNG)

**Advantages over Traditional Pose Estimation**

- BlazePose accurately localizes more keypoints, making it uniquely suited for fitness applications. 


- BlazePose achieves super-real-time performance, enabling it to run subsequent ML models, like face or hand tracking.

**How it works**

Blazepose uses a new topology of 33 human body keypoints. The inclusion of more keypoints is cruical for the subsequent application 
of domain-specific pose estimation models, like those for hands, face or feet.

![Pose detector](poseDetector.PNG)

It utilizes Google's two step detector-tracker ML pipeline. It then locates the region-of-interest within the frame. In order to perform the detection and tracking in the matter of a few milliseconds per frame, the Google researchers have used the persons face to obtain the strongest link to the neural network about the torso's position, hence making Blazepose a fast and lightweight pose detector.