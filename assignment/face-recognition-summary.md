**Face Recognition**

-----------------------------------------------------------------------------------------------------------------------------------------

Face Recognition is the method of identifying or ascertaining a person through a 
visual element of their face. This system is widely used as a security measure to verify a subject before allowing 
access to a application,system or service.

![Mobile Unlock](face_recog2.PNG)

**How it works**

Facial Recognition systems capture the live image from a camera in either a 2D or 3D representaion depending on its 
capabilities and design. (These systems can also work on static images, but this can cause security issues.)

Once the image is captured , it compares the features of the face with the one stored in its database by using 
AI/ML algorithms.

These algorithms are designed to focus on extracting the facial features of a person with a high accuracy and a low margin of error.

As a result these systems can operate with high standards in safety and security.


**Applications**

- Used as a primary or secondary option to unlock a device/application.

- Access to high security areas or restricted access areas in a building.

- Used in making payments/transactions.



